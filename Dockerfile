FROM keisukesatomi/fluentd:latest

MAINTAINER corginia keisukesatomi <mlh38048@yahoo.co.jp>

COPY ./td-agent.conf /etc/td-agent/td-agent.conf
COPY ./start.sh /root/start.sh

RUN chmod ugo+x /root/start.sh

#docker run -it --name=jvdata-fluentd-master -v /var/fluentd:/var/fluentd -v /etc/localtime:/etc/localtime:ro -v /var/log/nginx:/var/log/nginx:ro -v /var/lib/docker/containers:/var/lib/docker/containers:ro --entrypoint="/bin/bash" keisukesatomi/jvdata-fluentd-master:latest

EXPOSE 24243 24244 24245

ENTRYPOINT ["/root/start.sh"]
