#!/bin/bash
set -u
export PHALCON_FLUENT_DIR="${BASE_DIR}phalcon/"
export NGINX_FLUENT_DIR="${BASE_DIR}nginx/"

# make neccessary directories
if [ ! -e ${BASE_DIR} ]; then
  mkdir -p ${BASE_DIR}
fi

if [ ! -e ${PHALCON_FLUENT_DIR} ]; then
  mkdir -p ${PHALCON_FLUENT_DIR}
fi

if [ ! -e ${NGINX_FLUENT_DIR} ]; then
   mkdir -p ${NGINX_FLUENT_DIR}
fi

# all permitted
chmod 777 -R ${BASE_DIR}

td-agent
